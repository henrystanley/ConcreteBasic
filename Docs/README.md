# Concrete Basic

Concrete Basic is a programming language which seeks to carry on the tradition of the BASIC family of languages.
It has an easy to understand semantics and syntax which is designed to resemble natural languages.



# Installation

## Dependencies

To build the Concrete Basic interpreter you'll need a working C# compiler.
The interpreter uses only functions found in the StdLib, thus no further packages are required.
A valid installation of GNU Make is also required to orchestrate the build process.
The current makefile works only on UNIX based systems, but Windows support could be easily added in the future.


## Building

To build the interpreter simply enter the main directory and run:

    $ make

Installation of the interpreter can further be achieved with:

    $ sudo make install

Once install the interpreter can be evoked with `cbi`:

    $ cbi Examples/Primes.cb



# Documentation

Documentation can be found in the "Docs" directory.

For an introduction to the language try "GUIDE.md".
An overview of the interpreter source code can be found in "SRCDOC.md".
The "GRAMMER.txt" file contains a not-quite-Backus-Naur form specification of the language grammar.



# Examples

Many example Concrete Basic programs can be found in the "Examples" directory.
To run all such programs use:

    $ make examples
