
# Concrete Basic Source Code Overview

The Concrete Basic interpreter is on the whole a fairly simple program.
The interpretation process can be broken into 3 steps:
  
  - Lexing
  - Parsing
  - Evaluation

This overview will address each of these steps, and explain how they are implemented.
For more details consider reading the inline comments embedded in the source code.


## Lexing

The first two steps in the interpretation process are dedicated merely to transforming
a sequence of bytes representing a Concrete Basic Program's source code into an Abstract
Syntax Tree (AST). This transformation is divided into the Lexing and Parsing steps.

The Lexing step involves reading through the sequence of characters and returns a new sequence
of "Tokens". A Token represents a distict chunk of characters from the source code, for example
an identifier token or a comma token.

All the lexing code for the interpreter can be found in the file: "Src/Lexing.cs".
One of the first classes definined in this file, is the "Token" class.
Each Token is an object which holds the sequence of characters associated with it, along with
information about its location in the source code and the type of token it is.
Location information is stored using the "TextPos" class which holds on to which line and position
along the line a token was found. The types of tokens are represented by members of the Enum "TokenType".

A source file is represented in the interpreter with a "LexSrc" object. This object stores the source
file's text, an index into this sequence of characters, and a TextPos object corresponding with the index.
The most important methods for this class are:

- Clone(), which returns a duplicate LexSrc object.
- GetToken(TokenType type), which attempts to lex the given type of token from the source text.
- GetRegex(string rStr), which attempts to match the given RegEx against the current position
  in the source text. If this match succeeds then the position is advanced and the match is returned.

Lexing is performed by repeatedly asking a LexSrc for tokens of a specific type. First whitespace characters
and comments are removed from the front of the source. If such a token can be lexed then the token is
returned and the position in the source is advanced. When lexing the given type of token is impossible the
GetToken method throws a "LexException". This exception class is a subclass of the more general
"InputException", which encompasses both Lexing and Parsing errors.


## Parsing

The parsing step takes a sequence of tokens and folds them into an AST. Parsing code for the interpreter
can be found in "Src/Parsing.cs". There are many ways of implementing a parser, but the method utilized
here is call "Recursive Decent Parsing". In earlier builds an attempt was made to implement a Parser Combinator
library in C# modeled after the Parsec Haskell library, however this would eventually turn out to be a failure
because C#'s type system is insufficently powerful to make parsing with this method concise.

Recursive Decent Parsing relies on a myriad of functions which each attempt to parse a valid AST node from
a SrcText object. In the CB interpreter these are written as static methods of the static class "Parsers".
Some parser functions are standalone, such as the parser for numeric literals or the "TRUE" keyword. Others
employ repeated calls to other parser functions or disjunctions between a set of parsers, for example the
parser for the arguments of a function.

Each parser function takes a reference to a LexSrc and tries to return the AST node it corresponds with.
When such a structure cannot be parsed, a ParserException or ParserDisjunctionException will be thrown.
Both of these exception types can nest other exceptions inside themselves, which means that when a parsing
error occurs a cascade of exceptions is returned. This allows for more descriptive error messages upon
parsing failure. Before an attempt to parse is made, the SrcText input is cloned. This prevents a parser
function from consuming characters if the parse turns out to be impossible.


## Evaluation

Once the source text has been transformed into an Abstract Syntax Tree, the AST must be evaluated.
Faster interpreters like those for Python or C# itself output bytecode from the AST and then execute
that within a VM. The Concrete Basic interpreter however does not employ such a method, instead it
evaluates the AST directly. This leads to worse performance compared to languages which use a bytecode
VM, but also results in drastically less code being necessary to implment evaluation.

To begin understanding the evaluation process we must first look in "Src/AST.cs". This file contains
the class definitions for the various types of AST nodes. All AST nodes are decendents of the abstract
class ASTNode. This abstract class implements two methods, "Eval" and "Dump". Dump is used only for
debugging, but Eval is much more interesting. The Eval method for an ASTNode takes in an enviroment
object ("Env") and returns a new expression. Nodes often carry other nodes within themselves, like a
Program node which is merely a list of Statment nodes. When Eval is called on one node this node calls 
the method for all of its children as well. In this way the evaluation trickles down throughout the tree.

The simplest nodes are literal values like numbers, strings, and boolean values. These nodes simply
return themselves when Eval is called. Slightly more complicated are nodes which depend upon the enviroment
for their value, like Variable expressions or Let Statements.

The "Env" class can be found in the file "Src/Eval.cs". Enviroment objects are essentially just HashMaps
with pointers to further Enviroments. In this manner they form a stack of bindings, which can be pushed to
and popped from as layers of scope are entered and left during evaluation.

The evaluation of a "FunctionCall" object can sometimes result in a call to a Built in Function. These
BIFs are are found in the "Src/Eval.cs" file. The static class "BIFDispatch" holds a static dictionary
relating BIF names to their corresponding BIF objects. This class contains a static method "GetBIF" which
retrieves a static BIF object of the desired function. Each BIF has a single method "Call" which takes
a list of arguments and an enviroment and produces a new expression.
