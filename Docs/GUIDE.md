
# An Introduction to Concrete Basic


## Hello World

Concrete Basic is a simple dynamically typed programming language.
It is a procedural language like the other members of the Basic family.

In this guide we shall attempts to cover all the major parts of the language,
mostly in the form of short snippets of example code.

As is tradition the first program we shall write in CB is a simple script
which prints "Ahoj světe!" to the console:

    print("Ahoj světe!\n")

Here we demonstrate the use of the `print` function, which takes a string and prints it without a newline.
`print` is a built-in-function, meaning it is a function defined in the Concrete Basic standard library.
All keywords in the language as well as BIFs are case insensitive, all other language structures
such as variable names are case sensitive.

We can also observe from our simple program that string literals in CB are written between double quotes.
All of the standard escape sequences from C string literals such as '\n', '\t', and '\\' are supported.

One more thing to add before we move forward is that comments can be typed in CB with a '#':

    # This is a comment
    print("cat") # This prints the word 'cat'



## Numbers and Mathematical Expressions

All numbers in Concrete Basic are represented by double floating point values.

    print(str((5 * 3) + -4.7))

Here you can see a simple example of some mathematical expressions.
Also present is the `str` BIF, which converts any expression into a string.

Available to use in math expressions are the following operators:

    print(str(4 + 3))
    print(str(4 - 3))
    print(str(4 * 3))
    print(str(4 / 3))
    print(str(4 % 3))



## Boolean Expressions

Boolean literal values are produced from the `true` and `false` keywords:

    print(str(true & false))
    print(str(true | (false & false)))

Here you can see that boolean values can be used in the logical expressions `&` and `|`.

Boolean values are also the product of comparison functions:

    print(str(4 = 5))
    print(str(4 < 5))
    print(str(4 > 5))
    print(str(4 != 5))



## Variables

Variables may be defined by using a let-statement:

    let catSound = "meow"
    print(catSound)

The scope of variables is defined lexically.
Variables in closer scope shadow variables above.



## Functions

Functions are values in Concrete Basic.
A literal function may be written like so:

    fn(x) x + x end

Functions may include many statements, but only the value of the last
statement is returned.

    fn (a, b)
      print(a)
      print(b)
    end

Functions may be used as arguments to other functions or
as assignment values:

    print(str(fn(x, y) x + y end))
    let subtract = fn(a, b)
      a - b
    end
    print(str(subtract(2, 1.3))

A return-statement can be used to return a value immediatly from a function:

    let five = fn
      return 5
      print("I never get printed, oh well...")
    end
    print(str(five()))

Operators may be defined just like normal functions:

    let <~> = fn(left, right)
      return (left ~ ": " ~ right) # The '~' operator concatenates strings
    end
    print("a" <~> "b")



## Conditional Statements

Conditional statements can be acheived using an if-statement:

    let x = 4
    if x = 5 then
      print("x is 5")
    end

Additionally, an else-clause may be included:

    let y = 5
    if y != 5 then
      print("y isn't 5")
    end else
      print("y seems to be 5")
    end



## Looping Statements

To loop the language also includes a while-statment:

    let a = 4
    while a > 0 do
      print(str(a))
      let a = a - 1
    end

For looping specifically through ranges of numbers a for-loop may also be used:

    for i from 0 to 100 do
      for j from i to 0 do
        print(str(j))
      end
    end


