EXECUTABLE=cbi

SRC=$(wildcard ./Src/*.cs)

EXAMPLES=$(wildcard ./Examples/*.cb)


all: $(EXECUTABLE)

$(EXECUTABLE): $(SRC)
	mkdir -p Bin
	mcs $(SRC) -out:Bin/$(EXECUTABLE)

install: $(EXECUTABLE)
	cp ./Bin/$(EXECUTABLE) /usr/bin/$(EXECUTABLE)

clean:
	rm -rf ./Bin

examples: $(EXAMPLES) $(EXECUTABLE)
	for e in $(EXAMPLES) ; do \
		printf "### RUNNING EXAMPLE: %s\n" "$$e" ; \
		printf "# PROGRAM:\n" ; \
		cat "$$e" ; \
		printf "\n\n# OUTPUT:\n" ; \
		./Bin/$(EXECUTABLE) "$$e" ; \
		printf "\n\n\n" ; \
	done
