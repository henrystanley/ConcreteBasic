
using System;
using AST;
using Lexing;
using System.Collections.Generic;

namespace Parsing {

    /* Parser Exception Values */

    // Represents a parser failure
    class ParserException : InputException {
        // Properties
        private InputException SubException;

        // Init
        public ParserException() : base() {}
        public ParserException(string message, TextPos errorPos) : base(message, errorPos) {}
        public ParserException(string message, TextPos errorPos, InputException e) : base(message, errorPos) {
            SubException = e;
        }

        // Printing
        public override void PrintError(int depth = 0) {
             Console.Write((new String(' ', depth)) + "|p} ");
             Console.WriteLine("Parsing error at {0}: {1}", ErrorPos.ToString(), Message);
             if (SubException != null) SubException.PrintError(depth + 1);
        }
    }

    // Represents a failure between the disjunction of several parsers
    class ParserDisjunctionException : InputException {
        // Properties
        private List<InputException> SubExceptions;

        // Init
        public ParserDisjunctionException() : base() {
            SubExceptions = new List<InputException>();
        }
        public ParserDisjunctionException(string message, TextPos errorPos) : base(message, errorPos) {
            SubExceptions = new List<InputException>();
        }
        
        // Add sub-exception
        public void AddSubException(InputException e) {
            SubExceptions.Add(e);
        }

        // Printing
        public override void PrintError(int depth = 0) {
             Console.Write((new String(' ', depth)) + "|+} ");
             Console.WriteLine("Parsing error at {0}, no valid parsing options: {1}", ErrorPos.ToString(), Message);
             foreach (InputException e in SubExceptions) e.PrintError(depth + 1);
        }
    }


    /* Parsers */

    static class Parsers {
        // Parser for a number literal
        public static Number NumberP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                Token t = input_.GetToken(TokenType.Num);
                input = input_;
                return Number.MakeFromString(t.Chars);
            } catch (InputException e) {
                throw new ParserException("Could not parse valid number literal", input.TextPosition);
            }
        }

        // Parser for a string literal
        public static Str StrP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                Token t = input_.GetToken(TokenType.Str);
                input = input_;
                return Str.MakeFromString(t.Chars);
            } catch (InputException e) {
                throw new ParserException("Could not parse valid string literal", input.TextPosition);
            }
        }

        // Parser for the 'nil' keyword
        public static NilExpr NilP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.NilKeyword);
                input = input_;
                return new NilExpr();
            } catch (InputException e) {
                throw new ParserException("Could not parse valid nil literal", input.TextPosition);
            }
        }

        // Parser for the 'true' keyword
        public static TrueExpr TrueP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.TrueKeyword);
                input = input_;
                return new TrueExpr();
            } catch (InputException e) {
                throw new ParserException("Could not parse valid true literal", input.TextPosition);
            }
        }

        // Parser for the 'false' keyword
        public static FalseExpr FalseP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.FalseKeyword);
                input = input_;
                return new FalseExpr();
            } catch (InputException e) {
                throw new ParserException("Could not parse valid false literal", input.TextPosition);
            }
        }

        // Parser for a function literal
        public static UnboundClosure FnP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.FnKeyword);
                List<string> args = new List<string>();
                if (input_.CanGetToken(TokenType.LParen)) {
                    input_.GetToken(TokenType.LParen);
                    for (;;) {
                        if (input_.CanGetToken(TokenType.RParen)) {
                            input_.GetToken(TokenType.RParen);
                            break;
                        }
                        if (args.Count > 0) input_.GetToken(TokenType.Comma);
                        args.Add(input_.GetToken(TokenType.Ident).Chars);
                    }
                }
                Program body = BlockP(ref input_);
                input = input_;
                return new UnboundClosure(args, body);
            } catch (InputException e) {
                throw new ParserException("Could not parse valid function literal", input.TextPosition, e);
            }
        }

        // Parser for literal values
        public static Expr LiteralP(ref LexSrc input) {
            ParserDisjunctionException disjErr = new ParserDisjunctionException("Could not parse literal expression", input.TextPosition);
            LexSrc input_ = input.Clone();
            try {
                Number val = NumberP(ref input_);
                input = input_;
                return val;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Str val = StrP(ref input_);
                input = input_;
                return val;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                UnboundClosure val = FnP(ref input_);
                input = input_;
                return val;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                NilExpr val = NilP(ref input_);
                input = input_;
                return val;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                TrueExpr val = TrueP(ref input_);
                input = input_;
                return val;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                FalseExpr val = FalseP(ref input_);
                input = input_;
                return val;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            throw disjErr;
        }

        // Parser for a parenthesised expression
        public static Expr ParenP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.LParen);
                Expr expr = ExprP(ref input_);
                input_.GetToken(TokenType.RParen);
                input = input_;
                return expr;
            } catch (InputException e) {
                throw new ParserException("Could not parse expr in parentheses", input.TextPosition, e);
            }
        }

        // Parser for an operator call
        public static FunctionCall OpCallP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                List<Expr> args = new List<Expr>();
                args.Add(ExprP(ref input_, parseOperator: false));
                Token opName = input_.GetToken(TokenType.OpIdent);
                args.Add(ExprP(ref input_));
                input = input_;
                return new FunctionCall(opName.Chars, args);
            } catch (InputException e) {
                throw new ParserException("Could not parse operator call", input.TextPosition, e);
            }
        }

        // Parser for a variable expression
        public static Var VarP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                Token var = input_.GetToken(TokenType.Ident);
                input = input_;
                return new Var(var.Chars);
            } catch (InputException e) {
                throw new ParserException("Could not parse variable", input.TextPosition, e);
            }
        }

        // Parser for a function call
        public static FunctionCall FnCallP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                List<Expr> args = new List<Expr>();
                Token fnName = input_.GetToken(TokenType.Ident);
                input_.GetToken(TokenType.LParen);
                for (;;) {
                    if (input_.CanGetToken(TokenType.RParen)) {
                        input_.GetToken(TokenType.RParen);
                        input = input_;
                        return new FunctionCall(fnName.Chars, args);
                    }
                    if (args.Count >= 1) input_.GetToken(TokenType.Comma);
                    args.Add(ExprP(ref input_));
                }
            } catch(InputException e) {
                throw new ParserException("Could not parse function call", input.TextPosition, e);
            }
        }

        // Parser for expressions
        public static Expr ExprP(ref LexSrc input, bool parseOperator = true) {
            ParserDisjunctionException disjErr = new ParserDisjunctionException("Could not parse expression", input.TextPosition);
            LexSrc input_ = input.Clone();
            if (parseOperator) {
                try {
                    Expr expr = OpCallP(ref input_);
                    input = input_;
                    return expr;
                } catch (InputException e) {
                    disjErr.AddSubException(e);
                }
            }
            try {
                Expr expr = LiteralP(ref input_);
                input = input_;
                return expr;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Expr expr = FnCallP(ref input_);
                input = input_;
                return expr;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Expr expr = VarP(ref input_);
                input = input_;
                return expr;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            
            try {
                Expr expr = ParenP(ref input_);
                input = input_;
                return expr;
            } catch (InputException e) {}
            throw disjErr;
        }

        // Parser for a block
        public static Program BlockP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                List<Statement> statements = new List<Statement>();
                for (;;) {
                    if (input_.CanGetToken(TokenType.EndKeyword)) {
                        input_.GetToken(TokenType.EndKeyword);
                        input = input_;
                        return new Program(statements);
                    }
                    statements.Add(StatementP(ref input_));
                }
            } catch(InputException e) {
                throw new ParserException("Could not parse block", input.TextPosition, e);
            }
        }

        // Parser for a let statement
        public static LetStatement LetP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.LetKeyword);
                Token varName;
                if (input_.CanGetToken(TokenType.Ident)) varName = input_.GetToken(TokenType.Ident);
                else varName = input_.GetToken(TokenType.OpIdent);
                input_.GetToken(TokenType.Equals);
                Expr varVal = ExprP(ref input_);
                input = input_;
                return new LetStatement(varName.Chars, varVal);
            } catch (InputException e) {
                throw new ParserException("Could not parse let statement", input.TextPosition, e);
            }
        }

        // Parser for an if statement
        public static IfStatement IfP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.IfKeyword);
                Expr cond = ExprP(ref input_);
                input_.GetToken(TokenType.ThenKeyword);
                Program thenBlock = BlockP(ref input_);
                Program elseBlock = null;
                if (input_.CanGetToken(TokenType.ElseKeyword)) {
                    input_.GetToken(TokenType.ElseKeyword);
                    elseBlock = BlockP(ref input_);
                }
                input = input_;
                return new IfStatement(cond, thenBlock, elseBlock);
            } catch (InputException e) {
                throw new ParserException("Could not parse if statement", input.TextPosition, e);
            }
        }

        // Parser for a while statement
        public static WhileStatement WhileP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.WhileKeyword);
                Expr cond = ExprP(ref input_);
                input_.GetToken(TokenType.DoKeyword);
                Program block = BlockP(ref input_);
                input = input_;
                return new WhileStatement(cond, block);
            } catch (InputException e) {
                throw new ParserException("Could not parse while statement", input.TextPosition, e);
            }
        }

        // Parser for a for statement
        public static ForStatement ForP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.ForKeyword);
                string varName = input_.GetToken(TokenType.Ident).Chars;
                input_.GetToken(TokenType.FromKeyword);
                Expr fromNum = ExprP(ref input_);
                input_.GetToken(TokenType.ToKeyword);
                Expr toNum = ExprP(ref input_);
                input_.GetToken(TokenType.DoKeyword);
                Program block = BlockP(ref input_);
                input = input_;
                return new ForStatement(varName, fromNum, toNum, block);
            } catch (InputException e) {
                throw new ParserException("Could not parse for statement", input.TextPosition, e);
            }
        }

        // Parser for a return statement
        public static ReturnStatement ReturnP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                input_.GetToken(TokenType.ReturnKeyword);
                Expr expr = ExprP(ref input_);
                input = input_;
                return new ReturnStatement(expr);
            } catch (InputException e) {
                throw new ParserException("Could not parse return statement", input.TextPosition, e);
            }
        }

        // Parser for an expression statement
        public static ExprStatement ExprStatementP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                Expr expr = ExprP(ref input_);
                input = input_;
                return new ExprStatement(expr);
            } catch (InputException e) {
                throw new ParserException("Could not parse expr statement", input.TextPosition, e);
            }
        }

        // Parser for statements
        public static Statement StatementP(ref LexSrc input) {
            ParserDisjunctionException disjErr = new ParserDisjunctionException("Could not parse statement", input.TextPosition);
            LexSrc input_ = input.Clone();
            try {
                Statement statement = LetP(ref input_);
                input = input_;
                return statement;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Statement statement = IfP(ref input_);
                input = input_;
                return statement;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Statement statement = WhileP(ref input_);
                input = input_;
                return statement;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Statement statement = ForP(ref input_);
                input = input_;
                return statement;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Statement statement = ReturnP(ref input_);
                input = input_;
                return statement;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            try {
                Statement statement = ExprStatementP(ref input_);
                input = input_;
                return statement;
            } catch (InputException e) {
                disjErr.AddSubException(e);
            }
            throw disjErr;
        }

        // Parser for a program
        public static Program ProgramP(ref LexSrc input) {
            LexSrc input_ = input.Clone();
            try {
                List<Statement> statements = new List<Statement>();
                for (;;) {
                    if (input_.CanGetToken(TokenType.EndOfInput)) {
                        input = input_;
                        return new Program(statements);
                    }
                    statements.Add(StatementP(ref input_));                }
            } catch(InputException e) {
                throw new ParserException("Could not parse program", input.TextPosition, e);
            }
        }

    }

}
