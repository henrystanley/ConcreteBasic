
using System;
using System.IO;
using AST;
using Parsing;
using Lexing;

namespace Interpreter {
    class Interpreter {
        // Program entry point
        public static void Main(string[] argv) {
            foreach (string progFile in argv) InterpretFile(progFile);
        }

        // Reads file and attempts to parse and run program
        public static void InterpretFile(string filename) {
            string progText = File.ReadAllText(filename);
            LexSrc src = new LexSrc(progText);
            try {
                Program prog = Parsers.ProgramP(ref src);
                try {
                    prog.Eval(new Env());
                } catch (EvalException ee) {
                    throw ee; // TODO: Placeholder
                }
            } catch (InputException ie) {
                ie.PrintError();
            }
        }
    }
}