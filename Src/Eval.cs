using System;
using System.Collections.Generic;

namespace AST {

    /* Exceptions */

    // All exceptions that can occur in evaluation are subclasses of this class
    class EvalException : Exception {
        // Init
        public EvalException() : base() {}
        public EvalException(string message) : base(message) {}
        public EvalException(string message, System.Exception inner) : base(message, inner) {}
        protected EvalException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) {}
    }


    /* Evaluation Enviroment */

    // Enviroment for variable bindings
    class Env {
        // Properties
        private Dictionary<string, Expr> Bindings;
        private Env Inner;

        // Init
        public Env(Env inner) {
            Bindings = new Dictionary<string, Expr>();
            Inner = inner;
        }
        public Env() : this(null) {}

        // Cloning
        public Env Clone() {
            Env clone = new Env(Inner);
            foreach (var bind in Bindings) {
                clone.AssignVar(bind.Key, bind.Value);
            }
            return clone;
        }

        // Check if a bound var exists
        public bool IsBound(string var) {
            if (Inner != null && Inner.IsBound(var)) return true;
            return Bindings.ContainsKey(var);
        }

        // Assignment
        public void AssignVarShadow(string var, Expr val) {
            Bindings[var] = val;
        }
        public void AssignVar(string var, Expr val) {
            bool subAssign = this.SubAssignVar(var, val);
            if (!subAssign) Bindings[var] = val;
        }
        public bool SubAssignVar(string var, Expr val) {
            if (Bindings.ContainsKey(var)) {
                Bindings[var] = val;
                return true;
            }
            if (Inner != null) return Inner.SubAssignVar(var, val);
            return false;
        }

        // Variable call
        public Expr GetVar(string var) {
            if (Bindings.ContainsKey(var)) {
                return Bindings[var];
            } else if (Inner != null) {
                return Inner.GetVar(var);
            }
            string errMsg = string.Format("Variable \"{0}\" is unbound", var);
            throw new EvalException(errMsg);
        }
    }


    /* Built In Functions (BIFs) */

    // A built in function
    abstract class BIF {
        // Built in functions provide a method "Call" which calls the function with the given arguments
        abstract public Expr Call(List<Expr> args, Env e);
    }

    // Static class which provides dispatch for BIFs
    static class BIFDispatch {
        // Properties
        private static Dictionary<string, BIF> BIFs = new Dictionary<string, BIF> {
            { "print", new PrintF() },
            { "str", new StrF() },
            { "~", new StrConcatF() },
            { "+", new AddF() },
            { "-", new SubF() },
            { "*", new MulF() },
            { "/", new DivF() },
            { "%", new ModF() },
            { "=", new EQF() },
            { "!=", new NEQF() },
            { "<", new LTF() },
            { ">", new GTF() },
            { "&", new AndF() },
            { "|", new OrF() },
        };

        // Check if a given function name has a corresponding BIF
        public static bool IsBIF(string funcName) {
            return BIFs.ContainsKey(funcName.ToLower());
        }

        // Get a BIF
        public static BIF GetBIF(string funcName) {
            return BIFs[funcName.ToLower()];
        }

    }

    class PrintF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 1 && args[0] is Str) {
                Console.Write(((Str)args[0]).Val);
                return new NilExpr();
            } else {
                throw new EvalException("Type error: print :: String -> Nil");
            }
        }
    }

    class StrF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 1) {
                string converted = "???";
                if (args[0] is Str) return args[0];
                if (args[0] is Number) converted = ((Number)args[0]).Val.ToString();
                if (args[0] is NilExpr) converted = "nil";
                if (args[0] is TrueExpr) converted = "true";
                if (args[0] is FalseExpr) converted = "false";
                if (args[0] is Closure) converted = string.Format("Closure<{0}>", args[0].GetHashCode());
                return new Str(converted);
            } else {
                throw new EvalException("Type error: str :: Expr -> String");
            }
        }
    }

    class StrConcatF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Str && args[1] is Str) {
                string result = ((Str)args[0]).Val + ((Str)args[1]).Val;
                return new Str(result);
            } else {
                throw new EvalException("Type error: ~ :: String, String -> String");
            }
        }
    }

    class AddF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                double result = ((Number)args[0]).Val + ((Number)args[1]).Val;
                return new Number(result);
            } else {
                throw new EvalException("Type error: + :: Number, Number -> Number");
            }
        }
    }

    class SubF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                double result = ((Number)args[0]).Val - ((Number)args[1]).Val;
                return new Number(result);
            } else {
                throw new EvalException("Type error: - :: Number, Number -> Number");
            }
        }
    }

    class MulF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                double result = ((Number)args[0]).Val * ((Number)args[1]).Val;
                return new Number(result);
            } else {
                throw new EvalException("Type error: * :: Number, Number -> Number");
            }
        }
    }

    class DivF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                if ((((Number)args[0]).Val) == 0 || (((Number)args[1]).Val) == 0) {
                    throw new EvalException("Div by zero error");
                } 
                double result = ((Number)args[0]).Val / ((Number)args[1]).Val;
                return new Number(result);
            } else {
                throw new EvalException("Type error: / :: Number, Number -> Number");
            }
        }
    }

    class ModF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                double result = ((Number)args[0]).Val % ((Number)args[1]).Val;
                return new Number(result);
            } else {
                throw new EvalException("Type error: % :: Number, Number -> Number");
            }
        }
    }

    class EQF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                if(((Number)args[0]).Val == ((Number)args[1]).Val) {
                    return new TrueExpr();
                }
                return new FalseExpr();
            } else {
                throw new EvalException("Type error: = :: Number, Number -> Boolean");
            }
        }
    }

    class NEQF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                if(((Number)args[0]).Val != ((Number)args[1]).Val) {
                    return new TrueExpr();
                }
                return new FalseExpr();
            } else {
                throw new EvalException("Type error: = :: Number, Number -> Boolean");
            }
        }
    }

    class LTF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                if(((Number)args[0]).Val < ((Number)args[1]).Val) {
                    return new TrueExpr();
                }
                return new FalseExpr();
            } else {
                throw new EvalException("Type error: < :: Number, Number -> Boolean");
            }
        }
    }

    class GTF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2 && args[0] is Number && args[1] is Number) {
                if(((Number)args[0]).Val > ((Number)args[1]).Val) {
                    return new TrueExpr();
                }
                return new FalseExpr();
            } else {
                throw new EvalException("Type error: > :: Number, Number -> Boolean");
            }
        }
    }

    class AndF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2) {
                if(args[0].Truthiness(e) && args[0].Truthiness(e)) {
                    return new TrueExpr();
                }
                return new FalseExpr();
            } else {
                throw new EvalException("Type error: & :: Expr, Expr -> Boolean");
            }
        }
    }

    class OrF : BIF {
        public override Expr Call(List<Expr> args, Env e) {
            if (args.Count == 2) {
                if(args[0].Truthiness(e) || args[0].Truthiness(e)) {
                    return new TrueExpr();
                }
                return new FalseExpr();
            } else {
                throw new EvalException("Type error: | :: Expr, Expr -> Boolean");
            }
        }
    }

}