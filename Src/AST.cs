
using System;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AST {

    /* Abstract Syntax Tree */

    // A class for language structures
    abstract class ASTNode {
        // All members must provide a method which evaluates the given object relative to a provided binding enviroment
        abstract public Expr Eval(Env e);
        // Additionally the 'Dump' method must be implemented to allow for printing of raw AST nodes
        abstract public void Dump(int depth);
    }

    // A Program represents a sequence of Statement elements
    class Program : ASTNode {
        // Properties
        private List<Statement> Body;

        // Init
        public Program(List<Statement> body) {
            Body = body;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            Env blockEnv = new Env(e);
            Expr returnVal = new NilExpr();
            foreach (Statement s in Body) {
                returnVal = s.Eval(blockEnv);
                if (returnVal is ReturnExpr) {
                    return returnVal;
                }
            }
            return returnVal;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "Program:");
            foreach (Statement s in Body) s.Dump(depth + 1);
        }
    }


    /* Language Statements */

    // A Statement is a unit of execution
    abstract class Statement : ASTNode {}

    // A Let statement binds an Expression to a Variable
    class LetStatement : Statement {
        // Properties
        private String VarName;
        private Expr VarVal;

        // Init
        public LetStatement(String varName, Expr varVal) {
            VarName = varName;
            VarVal = varVal;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            Expr assignVal = VarVal.Eval(e);
            e.AssignVar(VarName, assignVal);
            return assignVal;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "Let Statement:");
            Console.Write(new String('\t', depth+1));
            Console.WriteLine("Ident: '{0}'", VarName);
            Console.WriteLine(new String('\t', depth+1) + "Expr:");
            VarVal.Dump(depth + 2);
        }
    }

    // An If statement uses a conditional Expression to branch Program execution
    class IfStatement : Statement {
        // Properties
        private Expr Condition;
        private Program ThenBlock;
        private Program ElseBlock;

        // Init
        public IfStatement(Expr cond, Program thenBlock, Program elseBlock) {
            Condition = cond;
            ThenBlock = thenBlock;
            ElseBlock = elseBlock;
        }
        public IfStatement(Expr cond, Program thenBlock) : this(cond, thenBlock, null) {}

        // Evaluation
        public override Expr Eval(Env e) {
            Expr condVal = Condition.Eval(e);
            if (condVal.Truthiness(e)) {
                return ThenBlock.Eval(e);
            } else {
                if (ElseBlock != null) return ElseBlock.Eval(e);
            }
            return new NilExpr();
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "If Statement:");
            Console.WriteLine(new String('\t', depth+1) + "Condition:");
            Condition.Dump(depth + 2);
            Console.WriteLine(new String('\t', depth+1) + "Then:");
            ThenBlock.Dump(depth + 2);
            if (ElseBlock != null) {
                Console.WriteLine(new String('\t', depth+1) + "Else:");
                ElseBlock.Dump(depth + 2);
            }
        }
    }

    // A While statement uses a conditional Expression to repeat Program execution
    class WhileStatement : Statement {
        // Properties
        private Expr Condition;
        private Program DoBlock;

        // Init
        public WhileStatement(Expr cond, Program doBlock) {
            Condition = cond;
            DoBlock = doBlock;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            Expr returnVal = new NilExpr();
            Expr condVal = Condition.Eval(e);
            while (condVal.Truthiness(e)) {
                returnVal = DoBlock.Eval(e);
                if (returnVal is ReturnExpr) return returnVal;
                condVal = Condition.Eval(e);
            }
            return returnVal;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "While Statement:");
            Console.WriteLine(new String('\t', depth+1) + "Condition:");
            Condition.Dump(depth + 2);
            Console.WriteLine(new String('\t', depth+1) + "Do:");
            DoBlock.Dump(depth + 2);
        }
    }

    // A For statement repeatedly executes a block for the given number of times
    class ForStatement : Statement {
        // Properties
        private string IterVar;
        private Expr FromNum;
        private Expr ToNum;
        private Program ForBlock;

        // Init
        public ForStatement(string iterVar, Expr fromNum, Expr toNum, Program forBlock) {
            IterVar = iterVar;
            FromNum = fromNum;
            ToNum = toNum;
            ForBlock = forBlock;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            Expr fromNum_ = FromNum.Eval(e);
            if (!(fromNum_ is Number)) {
                throw new EvalException("The from value in a for loop must be a Number");
            }
            Expr toNum_ = ToNum.Eval(e);
            if (!(toNum_ is Number)) {
                throw new EvalException("The to value in a for loop must be a Number");
            }
            Expr returnVal = new NilExpr();
            Number iter = new Number(((Number)FromNum).Val);
            Env e_ = new Env(e);
            if (((Number)fromNum_).Val < ((Number)toNum_).Val) {
                while (iter.Val <= ((Number)toNum_).Val) {
                    e_.AssignVarShadow(IterVar, iter);
                    returnVal = ForBlock.Eval(e_);
                    if (returnVal is ReturnExpr) return returnVal;
                    iter = new Number(iter.Val + 1);
                }
            } else {
                while (iter.Val >= ((Number)toNum_).Val) {
                    e_.AssignVarShadow(IterVar, iter);
                    returnVal = ForBlock.Eval(e_);
                    if (returnVal is ReturnExpr) return returnVal;
                    iter = new Number(iter.Val - 1);
                }
            }
            return returnVal;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "For Statement:");
            Console.WriteLine(new String('\t', depth+1) + string.Format("Var: {0}", IterVar));
            Console.WriteLine(new String('\t', depth+1) + "From:");
            FromNum.Dump(depth + 2);
            Console.WriteLine(new String('\t', depth+1) + "To");
            ToNum.Dump(depth + 2);
            Console.WriteLine(new String('\t', depth+1) + "Do:");
            ForBlock.Dump(depth + 2);
        }
    }

    // A Return statement produces a return expression which exits a function body immediately and returns a the given value
    class ReturnStatement : Statement {
        // Properties
        private Expr ReturnVal;

        // Init
        public ReturnStatement(Expr returnVal) {
            ReturnVal = returnVal;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return new ReturnExpr(ReturnVal.Eval(e));
        }
        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write((new String('\t', depth)) + "Return Statement:");
            ReturnVal.Dump(depth + 1);
        }
    }

    // Expression Statement to represent an expression value in a statement position
    class ExprStatement : Statement {
        // Properties
        private Expr Expression;

        // Init
        public ExprStatement(Expr expression) {
            Expression = expression;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return Expression.Eval(e);
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Expression.Dump(depth);
        }
    }


    /* Language Expressions */

    // Expressions are language structures with an associated value
    abstract class Expr : ASTNode {
        // Children of the Expr class must implement a method determining their boolean value
        abstract public bool Truthiness(Env e);
    }

    // Number Literal
    class Number : Expr {
        // Properties
        public double Val { get; private set; }

        // Init
        public Number(double val) {
            Val = val;
        }
        public static Number MakeFromString(string raw) {
            return new Number(double.Parse(raw));
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write(new String('\t', depth));
            Console.WriteLine("Num({0})", Val);
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return (Val != 0);
        }

    }

    // String Literal
    class Str : Expr {
        // Properties
        public string Val { get; private set; }

        // Init
        public Str(string val) {
            Val = val;
        }
        public static Str MakeFromString(string raw) {
            StringBuilder escaped = new StringBuilder();
            bool escMode = false;
            foreach (char c in raw) {
                if (escMode) {
                    switch (c) {
                        case 'n':
                            escaped.Append('\n');
                            break;
                        case 't':
                            escaped.Append('\t');
                            break;
                        case '\\':
                            escaped.Append('\\');
                            break;
                        default:
                            escaped.Append(c);
                            break;
                    }
                    escMode = false;
                } else if (c == '\\') escMode = true;
                else escaped.Append(c);
            }
            return new Str(escaped.ToString());
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write(new String('\t', depth));
            Console.WriteLine("Str('{0}')", Val);
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return true;
        }
    }

    // Variable
    class Var : Expr {
        // Properties
        private string VarName;

        // Init
        public Var(string varName) {
            VarName = varName;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return e.GetVar(VarName);
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write(new String('\t', depth));
            Console.WriteLine("Var({0})", VarName);
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return this.Eval(e).Truthiness(e);
        }
    }

    // Nil Keyword Literal
    class NilExpr : Expr {
        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write(new String('\t', depth));
            Console.WriteLine("NIL()");
        }
        
        // boolean value
        public override bool Truthiness(Env e) {
            return false;
        }
    }

    // False Keyword Literal
    class FalseExpr : Expr {
        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write(new String('\t', depth));
            Console.WriteLine("FALSE()");
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return false;
        }
    }

    // True Keyword Literal
    class TrueExpr : Expr {
        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.Write(new String('\t', depth));
            Console.WriteLine("TRUE()");
        }

        // boolean Value
        public override bool Truthiness(Env e) {
            return true;
        }
    }

    // Closure Literal (Before Evaluation)
    class UnboundClosure : Expr {
        // Properties
        private List<string> ArgVars;
        private Program Body;

        // Init
        public UnboundClosure(List<string> argVars, Program body) {
            ArgVars = argVars;
            Body = body;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return new Closure(ArgVars, Body, e);
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "Unbound Closure:");
            Console.WriteLine(new String('\t', depth+1) + "Vars:");
            foreach (string v in ArgVars) Console.Write("{0} ", v);
            Console.WriteLine(new String('\t', depth+1) + "Body:");
            Body.Dump(depth+2);
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return true;
        }
    }

    // Closure Literal (After Evaluation)
    class Closure : Expr {
        // Properties
        private List<string> ArgVars;
        private Program Body;
        private Env BoundEnv;

        // Init
        public Closure(List<string> argVars, Program body, Env boundEnv) {
            ArgVars = argVars;
            Body = body;
            BoundEnv = boundEnv;
        }
        
        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        public Expr Call(List<Expr> args) {
            if (ArgVars.Count != args.Count) {
                string errMsg = string.Format("Function takes {0} arguments but {1} have been given", ArgVars.Count, args.Count);
                throw new EvalException(errMsg);
            }
            Env argEnv = new Env(BoundEnv);
            for (int i = 0; i < ArgVars.Count; i++) {
                argEnv.AssignVarShadow(ArgVars[i], args[i]);
            }
            Expr funcResult = Body.Eval(argEnv);
            if (funcResult is ReturnExpr) return ((ReturnExpr)funcResult).Expression;
            return funcResult;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "Closure:");
            Console.WriteLine(new String('\t', depth+1) + "Vars:");
            foreach (string v in ArgVars) Console.WriteLine("{0} ", v);
            Console.WriteLine(new String('\t', depth+1) + "Body:");
            Body.Dump(depth+2);
        }
        
        // boolean value
        public override bool Truthiness(Env e) {
            return true;
        }
    }

    // Call to a function
    class FunctionCall : Expr {
        // Properties
        private string FunctionName;
        private List<Expr> Args;
        
        // Init
        public FunctionCall(string name, List<Expr> args) {
            FunctionName = name;
            Args = args;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            List<Expr> evaledArgs = new List<Expr>();
            foreach (Expr arg in Args) {
                try {
                    evaledArgs.Add(arg.Eval(e));
                } catch (EvalException err) {
                    string errMsg = string.Format("Error evaluating function '{0}', could not evaluate arguments:", FunctionName);
                    throw new EvalException(errMsg, err);
                }
            }
            if (BIFDispatch.IsBIF(FunctionName)) return BIFDispatch.GetBIF(FunctionName).Call(evaledArgs, e);
            if (e.IsBound(FunctionName)) {
                Expr func = e.GetVar(FunctionName);
                if (func is Closure) {
                    try {
                      return ((Closure)func).Call(evaledArgs);
                    } catch (EvalException err) {
                        string errMsg = string.Format("Error evaluating function {0}:", FunctionName);
                        throw new EvalException(errMsg, err);
                    }
                } else {
                    string errMsg = string.Format("Variable \"{0}\" is not a function", FunctionName);
                    throw new EvalException(errMsg);
                }
            } else {
                string errMsg = string.Format("No such function \"{0}\"", FunctionName);
                throw new EvalException(errMsg);
            }
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + string.Format("Function call '{0}':", FunctionName));
            Console.WriteLine(new String('\t', depth+1) + "Args:");
            foreach (Expr e in Args) e.Dump(depth+2);
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return this.Eval(e).Truthiness(e);
        }
    }

    // Return value from block
    class ReturnExpr : Expr {
        // Properties
        public Expr Expression { get; private set; }

        // Init
        public ReturnExpr(Expr expression) {
            Expression = expression;
        }

        // Evaluation
        public override Expr Eval(Env e) {
            return this;
        }

        // Dumping
        public override void Dump(int depth = 0) {
            Console.WriteLine(new String('\t', depth) + "Return expr:");
            Expression.Dump(depth+1);
        }

        // boolean value
        public override bool Truthiness(Env e) {
            return Expression.Truthiness(e);
        }
    }

}