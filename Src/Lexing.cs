
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Lexing {
 
    /* Lexing Exception Values */

    // All parsing and lexing functions throw children of this exception
    class InputException : Exception {
        // Properties
        public TextPos ErrorPos;

        // Init
        public InputException() : base() {}
        public InputException(string message, TextPos errorPos) : base(message) {
            ErrorPos = errorPos;
        }

        // Printing
        public virtual void PrintError(int depth = 0) {
             Console.Write((new String(' ', depth)) + "|} ");
             Console.WriteLine("Input error at {0}: {1}", ErrorPos.ToString(), Message);
        }
    }

    // All exceptions that can occur in lexing code are subclasses of this class
    class LexException : InputException {
        // Init
        public LexException() : base() {}
        public LexException(string message, TextPos errorPos) : base(message, errorPos) {}

        // Printing
        public override void PrintError(int depth = 0) {
             Console.Write((new String(' ', depth)) + "|l} ");
             Console.WriteLine("Lexing error at {0}: {1}", ErrorPos.ToString(), Message);
        }
    }


    /* Lexing Return Values */

    public enum TokenType {
        Num,
        Str,
        Ident,
        OpIdent,
        Semicolon,
        Comma,
        Equals,
        RParen,
        LParen,
        EndKeyword,
        LetKeyword,
        IfKeyword,
        ThenKeyword,
        ElseKeyword,
        WhileKeyword,
        DoKeyword,
        ForKeyword,
        FromKeyword,
        ToKeyword,
        NilKeyword,
        TrueKeyword,
        FalseKeyword,
        FnKeyword,
        ReturnKeyword,
        EndOfInput,
    }
 
    // A token represents a single lexeme from the input
    // It carries its string value, type, and location in the source file
    class Token {
        // Properties
        public string Chars;
        public TokenType Type;
        public TextPos Pos;

        // Init
        public Token(string chars, TokenType type, TextPos pos) {
            Chars = chars;
            Type = type;
            Pos = pos;
        }
    }


    /* Lexing */

    // A TextPos is used to keep track of the position of a given token
    class TextPos {
        // Properties
        public int LinePos { get; private set; }
        public int LineNum { get; private set; }

        // Init
        public TextPos(int linePos, int lineNum) {
            LinePos = linePos;
            LineNum = lineNum;
        }
        public TextPos() : this(1, 1) {}

        // Cloning
        public TextPos Clone() {
            return new TextPos(LinePos, LineNum);
        }

        // Update positions
        public void Update(string consumedStr) {
            for (int i = 0; i < consumedStr.Length; i++) {
                LinePos++;
                if (consumedStr[i] == '\n') {
                    LinePos = 1;
                    LineNum++;
                }
            }
        }

        // Convert to string
        public override string ToString() {
            return string.Format("({0}:{1})", LineNum, LinePos);
        }
    }

    // A LexSrc represents a piece of input text to be lexed
    class LexSrc {
        // Properties
        private string Text;
        private int Pos;
        public TextPos TextPosition { get; private set; }

        // Init
        public LexSrc(string text, int pos, TextPos textPosition) {
            Text = text;
            Pos = pos;
            TextPosition = textPosition;
        }
        public LexSrc(string text) : this(text, 0, new TextPos()) {}

        // Cloning
        public LexSrc Clone() {
            return new LexSrc(Text, Pos, TextPosition.Clone());
        }

        // Text Matching
        public Match GetRegex(string rStr) {
            Regex r = new Regex(rStr);
            Match m = r.Match(Text, Pos);
            if (m.Success && m.Index == Pos) {
                Pos += m.Length;
                TextPosition.Update(m.Value);
                return m;
            } else {
                string errMsg = string.Format("Could not match regex against input: /{0}/", r.ToString());
                throw new LexException(errMsg, TextPosition);
            }
        }

        // See if end of text has been reached
        public bool AtEnd() {
            return ((Text.Length - 1) <= Pos);
        }

        // Attempts to lex the given TokenType
        public Token GetToken(TokenType type) {
            GetRegex(@"[\s\n\t]*"); // Ignore whitespace
            if (Pos < Text.Length && Text[Pos] == '#') GetRegex(@"#[^\n]*[\s\n\t]*"); // Ignore comments
            string TokenChars;
            switch (type) {
                case TokenType.Num:
                    TokenChars = GetRegex(@"-?[0-9]+(\.[0-9]+)?").Value;
                    break;
                case TokenType.Str:
                    TokenChars = GetRegex(@"""(((\\"")|[^""])+)""").Groups[1].Value;
                    break;
                case TokenType.Ident:
                    TokenChars = GetRegex(@"\w[^.,=(){}"";\s\n]*").Value;
                    break;
                case TokenType.OpIdent:
                    TokenChars = GetRegex(@"[+\-*/%<>=!@$^&?.:|~]+").Value;
                    break;
                case TokenType.Semicolon:
                    TokenChars = GetRegex(@";").Value;
                    break;
                case TokenType.Comma:
                    TokenChars = GetRegex(@",").Value;
                    break;
                case TokenType.Equals:
                    TokenChars = GetRegex(@"=").Value;
                    break;
                case TokenType.RParen:
                    TokenChars = GetRegex(@"\)").Value;
                    break;
                case TokenType.LParen:
                    TokenChars = GetRegex(@"\(").Value;
                    break;
                case TokenType.EndKeyword:
                    TokenChars = GetRegex(@"(END)|(end)").Value;
                    break;
                case TokenType.LetKeyword:
                    TokenChars = GetRegex(@"(LET)|(let)").Value;
                    break;
                case TokenType.IfKeyword:
                    TokenChars = GetRegex(@"(IF)|(if)").Value;
                    break;
                case TokenType.ThenKeyword:
                    TokenChars = GetRegex(@"(THEN)|(then)").Value;
                    break;
                case TokenType.ElseKeyword:
                    TokenChars = GetRegex(@"(ELSE)|(else)").Value;
                    break;
                case TokenType.WhileKeyword:
                    TokenChars = GetRegex(@"(WHILE)|(while)").Value;
                    break;
                case TokenType.DoKeyword:
                    TokenChars = GetRegex(@"(DO)|(do)").Value;
                    break;
                case TokenType.ForKeyword:
                    TokenChars = GetRegex(@"(FOR)|(for)").Value;
                    break;
                case TokenType.FromKeyword:
                    TokenChars = GetRegex(@"(FROM)|(from)").Value;
                    break;
                case TokenType.ToKeyword:
                    TokenChars = GetRegex(@"(TO)|(to)").Value;
                    break;
                case TokenType.NilKeyword:
                    TokenChars = GetRegex(@"(NIL)|(nil)").Value;
                    break;
                case TokenType.TrueKeyword:
                    TokenChars = GetRegex(@"(TRUE)|(true)").Value;
                    break;
                case TokenType.FalseKeyword:
                    TokenChars = GetRegex(@"(FALSE)|(false)").Value;
                    break;
                case TokenType.FnKeyword:
                    TokenChars = GetRegex(@"(FN)|(fn)").Value;
                    break;
                case TokenType.ReturnKeyword:
                    TokenChars = GetRegex(@"(RETURN)|(return)").Value;
                    break;
                case TokenType.EndOfInput:
                    if ((Text.Length - 1) <= Pos) TokenChars = "";
                    else throw new LexException("Not at end of input", TextPosition);
                    break;
                default:
                    throw new LexException("Unknown token type", TextPosition);
            }
            return new Token(TokenChars, type, TextPosition.Clone());
        }

        public bool CanGetToken(TokenType type) {
            LexSrc copy = Clone();
            try {
                copy.GetToken(type);
                return true;
            } catch (LexException e) {
                return false;
            }

        }
    }


}